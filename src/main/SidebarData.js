import React from "react"
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'

export const SidebarData = [
    {
        title: 'Home',
        path: '/',
        icon: <AiIcons.AiFillHome />,
        cName: 'nav-text'
    },
    {
        title: 'Film',
        path: '/film',
        icon: <FaIcons.FaFilm />,
        cName: 'nav-text'
    },
    {
        title: 'Bioskop',
        path: '/bioskop',
        icon: <FaIcons.FaTheaterMasks />,
        cName: 'nav-text'
    },
    {
        title: 'Edit Data',
        path: '/admin',
        icon: <FaIcons.FaCog />,
        cName: 'nav-text'
    },
]