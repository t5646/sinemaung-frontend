import React, { useContext } from "react";
import { Navbar, Container } from "react-bootstrap";
import { Switch, Route, useHistory } from "react-router";
import { Link } from "react-router-dom";
import './Main.css'
import { UserContext } from "../auth/UserContext";
import Cookies from "js-cookie";

const Header = () => {
    const { setIsLogin } = useContext(UserContext)
    let history = useHistory()

    const handleLogout = () => {
        setIsLogin(false)
        Cookies.remove('username')
        Cookies.remove('role')
        Cookies.remove('token')
        history.push('/login')
    }

    return(
        <Navbar>
            <Container>
                <Navbar.Brand><NavbarText /></Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                {
                    Cookies.get('token') !== undefined &&
                    <>
                        <Navbar.Text>
                            Welcome, {Cookies.get('username')}
                        </Navbar.Text>
                        <Navbar.Text onClick={handleLogout}>
                            Logout
                        </Navbar.Text>
                    </>
                }
                {
                    Cookies.get('token') === undefined &&
                    <>
                        <Link to="/login">
                            <Navbar.Text>
                                Login
                            </Navbar.Text>
                        </Link>
                    </>
                }
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

const NavbarText = () => {
    return(
      <Switch>
        <Route exact path="/">Dashboard</Route>
        <Route path="/film">Film</Route>
        <Route path="/bioskop">Bioskop</Route>
        <Route path="/pembelian-tiket">Pembelian</Route>
        <Route path="/admin">Edit</Route>
      </Switch>
    )
}

export default Header