import React, { useState, useContext } from 'react'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import { Link } from 'react-router-dom'
import { SidebarData } from './SidebarData'
import { SidebarDataUser } from './SidebarDataUser'
import Cookies from 'js-cookie'

const Sidebar = () => {
    const [sidebar, setSidebar] = useState(false)

    const showSidebar = () => {
        setSidebar(!sidebar)
    }

    return (
        <>
            <div className="sidebar-element">
                <Link to='#' className='menu-bars'>
                    <FaIcons.FaBars onClick={showSidebar} />
                </Link>
            </div>
            <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                <ul className='nav-menu-items' onClick={showSidebar}>
                    <li className='navbar-toggle'>
                        <Link to='#' className='menu-bars'>
                            <AiIcons.AiOutlineClose />
                        </Link>
                        </li>
                        {
                            Cookies.get('role') === "admin" &&
                            <>
                                {SidebarData.map((item, index) => {
                                return (
                                    <li key={index} className={item.cName}>
                                    <Link to={item.path}>
                                        {item.icon}
                                        <span className="sidebar-name">{item.title}</span>
                                    </Link>
                                </li>
                                );
                                })}
                            </>
                        }
                        {
                            Cookies.get('role') === "user" &&
                            <>
                                {SidebarDataUser.map((item, index) => {
                                return (
                                    <li key={index} className={item.cName}>
                                    <Link to={item.path}>
                                        {item.icon}
                                        <span className="sidebar-name">{item.title}</span>
                                    </Link>
                                </li>
                                );
                                })}
                            </>
                        }
                </ul>
            </nav>
        </>
    )
}

export default Sidebar