import React from "react";
import Header from "./Header";
import Sidebar from "./Sidebar";
import Routes from "./Routes";


const Main = () => {

    return (
        <div style={{ minHeight: '100vh' }}>
                <Header />
                <Sidebar />
                <Routes />
        </div>
    )
}

export default Main
