import React, { useContext, useEffect } from "react";
import {
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
// auth
import Register from "../auth/Register";
import Login from "../auth/Login";
import { UserContext } from "../auth/UserContext";
import Cookies from "js-cookie";
// film
import FilmList from "../content/film/FilmList";
import FilmDetail from "../content/film/FilmDetail";
import FilmTable from "../edit/film/FilmTable";
import FilmAddForm from "../edit/film/FilmAddForm";
import FilmEditForm from "../edit/film/FilmEditForm";
// bioskop
import BioskopList from "../content/bioskop/BioskopList";
import BioskopDetail from "../content/bioskop/BioskopDetail";
import BioskopTable from "../edit/bioskop/BioskopTable";
import BioskopForm from "../edit/bioskop/BioskopForm";
// jadwal
import Jadwal from "../content/jadwal/Jadwal";
import AdminJadwal from "../edit/jadwal/AdminJadwal";
import FormJadwal from "../edit/jadwal/FormJadwal";
// makanan
import MakananForm from "../edit/makanan/MakananForm";
import MakananFormEdit from "../edit/makanan/MakananFormEdit";
import MakananTable from "../edit/makanan/MakananTable";
import ListMakanan from "../content/makanan/ListMakanan";
// pembelian tiket
import PembelianTiket from "../content/pembelian/pembelianTiket";
import Receipt from "../content/pembelian/receipt";
// dashboard
import Dashboard from "../content/dashboard/Dashboard";
import AdminDashboard from "../edit/adminDashboard/AdminDashboard";

const Routes = () => {

    const { isLogin, setIsLogin } = useContext(UserContext)

    useEffect(() => {
        Cookies.get('token') !== undefined ? setIsLogin(true) : setIsLogin(false)
    })

    const LoginRoute = ({...props}) =>
        Cookies.get('token') !== undefined ? <Redirect to="/" /> : <Route {...props} />

    const PrivateRoute = ({...props}) =>
        Cookies.get('token') === undefined ? <Redirect to="/login" /> : <Route {...props} />

    const AdminRoute = ({...props}) =>
        Cookies.get('role') !== "admin" ? <Redirect to="/" /> : <Route {...props} />

    return(
        <Switch>
            <Route exact path="/" component={Dashboard}/>
            <AdminRoute exact path="/admin" component={AdminDashboard}/>
            {/* auth */}
            <LoginRoute exact path="/register" component={Register} />
            <LoginRoute exact path="/login" component={Login} />
            {/* film */}
            <PrivateRoute exact path="/film" component={FilmList} />
            <PrivateRoute path="/film/details/:idFilm" component={FilmDetail} />
            <AdminRoute exact path="/admin/film" component={FilmTable} />
            <AdminRoute path="/admin/film/create" component={FilmAddForm} />
            <AdminRoute path="/admin/film/update/:idFilm" component={FilmEditForm} />
            {/* bioskop */}
            <PrivateRoute exact path="/bioskop" component={BioskopList} />
            <PrivateRoute exact path="/bioskop/:idBioskop" component={BioskopDetail} />
            <AdminRoute exact path="/admin/bioskop" component={BioskopTable} />
            <AdminRoute path="/admin/bioskop/create" component={BioskopForm} />
            <AdminRoute path="/admin/bioskop/edit/:idFilm" component={BioskopForm} />
            {/* jadwal */}
            <PrivateRoute path="/bioskop/:idBioskop/jadwal" component={Jadwal} />
            <AdminRoute exact path="/admin/jadwal/"  component={AdminJadwal} />
            <AdminRoute path="/admin/jadwal/create" component={FormJadwal} />
            <AdminRoute path="/admin/jadwal/update/:idJadwal" component={FormJadwal} />
            {/* makanan */}
            <AdminRoute path="/admin/makanan/" exact component={MakananTable} />
            <AdminRoute path="/admin/makanan/create" exact component={MakananForm} />
            <AdminRoute path="/admin/makanan/update/:idMakanan" component={MakananFormEdit} />
            <PrivateRoute path="/jadwal/pembelian-tiket/:id/makanan/:id" component={ListMakanan}/>
            {/* pembelian */}
            <PrivateRoute exact path="/jadwal/pembelian-tiket/:id" component={PembelianTiket} />
            <PrivateRoute exact path="/jadwal/:idjadwal/receipt/:id" component={Receipt} />
        </Switch>
    )
}

export default Routes