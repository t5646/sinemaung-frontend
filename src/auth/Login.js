import React, { useState, useContext } from "react";
import './Auth.css';
import { useHistory } from "react-router";
import Cookies from "js-cookie";
import { UserContext } from "./UserContext";
import axios from "axios";

const Login = () => {
    const { isLogin, setIsLogin } = useContext(UserContext)
    const [input, setInput] = useState({
        username: "",
        password: ""
    })
    let history = useHistory()

    const changePage = () => {
        history.push('/register')
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({...input, [name] : value})
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        axios.post("https://sinemaung-backend.herokuapp.com/auth/login/", {
            username: input.username,
            password: input.password
        }).then(result => {
            let user = result.data.username
            let role = result.data.role
            let token = result.data.token
            Cookies.set('username', user, {expires: 1})
            Cookies.set('role', role, {expires: 1})
            Cookies.set('token', token, {expires: 1})
            history.push('/')
            setIsLogin(true)
        }).catch((err) => {
            alert('Invalid Credentials')
        })
    }
    
    return(
        <>
            <div className="login-form">
                <h3>Login</h3>
                <form onSubmit={handleSubmit}>
                    <table className="login-table">
                        <tr>
                            <td><label>Username: </label></td>
                            <td><input type="text" name="username" onChange={handleChange} value={input.username} required/></td>
                        </tr>
                        <tr>
                            <td><label>Password: </label></td>
                            <td><input type="password" name="password" onChange={handleChange} value={input.password} required/></td>
                        </tr>
                    </table>
                    <div className="centered-auth">
                        <button className="button-auth">Login</button>
                        <p>Don't have an account? <span className="blue-text" onClick={changePage}> Register here</span></p>
                    </div>
                </form>
            </div>
        </>
    )
}

export default Login