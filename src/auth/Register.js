import React, { useState } from "react";
import './Auth.css'
import { useHistory } from "react-router";
import axios from "axios";

const Register = () => {
    const [input, setInput] = useState({
        username: "", 
        password: "",
        password2: "",
        role: ""})
    let history = useHistory()

    const changePage = () => {
        history.push('/login')
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({...input, [name] : value})
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        if(input.password !== input.password2) {
            alert('Masukkan konfirmasi password dengan benar!')
        }

        axios.post("https://sinemaung-backend.herokuapp.com/auth/register/", {
            username: input.username,
            password: input.password,
            password2: input.password2,
            role: input.role
        }).then(() => {
                history.push('/login')
            }
        ).catch((err) => {
            alert(err)
        })
    }
    
    return(
        <>
            <div className="register-form">
                <h3>Register</h3>
                <form onSubmit={handleSubmit}>
                    <table className="register-table">
                        <tbody>
                            <tr>
                                <td><label>Username: </label></td>
                                <td><input type="text" name="username" onChange={handleChange} value={input.username} required/></td>
                            </tr>
                            <tr>
                                <td><label>Password: </label></td>
                                <td><input type="password" name="password" onChange={handleChange} value={input.password} required/></td>
                            </tr>
                            <tr>
                                <td><label>Konfirmasi Password: </label></td>
                                <td><input type="password" name="password2" onChange={handleChange} value={input.password2} required/></td>
                            </tr>
                            <tr>
                                <td><label>Pilih Role: </label></td>
                                <td className="input-role">
                                    <input type="radio" id="user" name="role" onChange={handleChange} value="user"/>
                                    <label htmlFor="user">User</label>
                                </td>
                                <td className="input-role">
                                    <input type="radio" id="admin" name="role" onChange={handleChange} value="admin"/>
                                    <label htmlFor="admin">Admin</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div className="centered-auth">
                        <button className="button-auth">Register</button>
                        <p>Already have an account? <span className="blue-text" onClick={changePage}> Login here</span></p>
                    </div>
                </form>
            </div>
        </>
    )
}

export default Register