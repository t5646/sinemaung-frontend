import React, { useEffect, useState } from "react"
import { useHistory } from "react-router";
import './FilmList.css'
import axios from "axios";

const FilmList = () => {
    const [film, setFilm] = useState([])
    let history = useHistory()

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://sinemaung-backend.herokuapp.com/film/film-list/`)
            setFilm(result.data.map(el => {
                return {
                    id: el.id,
                    title: el.title,
                    rating: el.rating,
                    duration: el.duration,
                    poster: el.poster
                }
            }))
        }
        fetchData()
    }, [])

    const handleScroll = () => {
        document.getElementById('film-list').scrollIntoView()
    }

    const goToDetails = (event) => {
        let index = parseInt(event.target.value)
        history.push(`/film/details/${index}`)
    }
    
    return(
        <>
            <div className="content-film">
                <div className="film-title">
                   <span className="word-now">Now</span> Playing
                   <div><button className="explore-more" onClick={handleScroll}>Explore More!</button></div>
                </div>

                <div className="film-list" id="film-list">
                    {
                        film.map((val, index) => {
                            return(
                                <div className="film-card" key={index}>
                                    <img src={`https://image.tmdb.org/t/p/w500${val.poster}`} alt="picture"/>
                                    <div className="container-film">
                                        <h4><strong>{val.title}</strong></h4>
                                        <p>Rating: {val.rating}/10</p>
                                        <p>{val.duration} Minutes</p>
                                        <button className="link-detail" value={val.id} onClick={goToDetails}>&#62;&#62; See Details</button>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}

export default FilmList