import React, { useState, useEffect } from "react"
import { useHistory } from "react-router"
import axios from "axios"

const FilmDetail = () => {
    const [title, setTitle] = useState("")
    const [desc, setDesc] = useState("")
    const [rating, setRating] = useState(0)
    const [duration, setDuration] = useState(0)
    const [poster, setPoster] = useState("")
    const [genre, setGenre] = useState([])
    const [namaBioskop, setNamaBioskop] = useState([])
    let history = useHistory()

    useEffect(() => {
        const fetchData = async () => {
            let tempUrl = window.location.href
            let tempId = tempUrl.substring(tempUrl.lastIndexOf('/')+1)
            // get detail film data
            const result = await axios.get(`https://sinemaung-backend.herokuapp.com/film/film-detail/${tempId}/`)
            let temp = []
            setTitle(result.data.title)
            setDesc(result.data.desc)
            setRating(result.data.rating)
            setDuration(result.data.duration)
            setPoster(result.data.poster)
            if(result) {
                result.data.genres.map(el => {
                    temp.push(el.name)
                })
                setGenre(temp)
            }

            // get list of bioskop
            const bioskopData = await axios.get(`https://sinemaung-backend.herokuapp.com/bioskop/get`)
            let tempBioskop = []
            bioskopData.data.map(el => {
                el.list_of_film.map(idx => {
                    if(idx.id == tempId) {
                        let res = {
                            id: el.id,
                            name: el.nama
                        }
                        tempBioskop = [...tempBioskop, res]
                    }
                })
            })
            console.log(tempBioskop);
            setNamaBioskop(tempBioskop)
        }
        fetchData()
    }, [])
    
    const changePage = (event) => {
        let index = event.target.value
        console.log(event.target)
        history.push(`/bioskop/${index}`)
    }

    const testBioskop = () => {
        console.log(namaBioskop);
    }

    return(
        <>
            <div className="content-details">
                <div className="film-detail">
                    <img src={`https://image.tmdb.org/t/p/w500${poster}`} alt="picture"/>
                    <div className="detail-jsaon">
                        <h2 onClick={testBioskop}>{title}</h2>
                        <div className="genres">
                            {
                                genre.map(el => {
                                    return (
                                        <span className="genre">{el}</span>
                                    )
                                })
                            }
                        </div>
                        <p>{desc}</p>
                        <div className="attributes">
                            <span>{duration} Minutes</span>
                            <span className="vl"/>
                            <span>{rating}/10</span>
                        </div>
                    </div>
                </div>

                <div className="bioskop-list-jsaon">
                    <h2>Bioskop yang menayangkan film ini</h2>
                    <div className="bioskops">
                        {
                            namaBioskop.map(el => {
                                return(
                                    <button onClick={changePage} value={el.id} className="bioskop">{el.name}</button>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default FilmDetail