import React, {useState, useEffect, useContext} from "react";
import './ListMakanan.css'
import {useHistory } from "react-router-dom";
import axios from "axios";
import {Button} from "react-bootstrap";


const ListMakanan = () => {
    const [makanan, setMakanan] = useState([])
      const [count, setCount] = useState(0);
      const [hargaSatuan, setHargaSatuan] = useState(0)
      const [listMakanan,setListMakanan]=useState([{"id":0,"harga":0}])
    const [idMakanan,setIdMakanan]=useState()
      let history = useHistory();

      useEffect(() => {
        fetchDataMakanan();
      }, [])
  
      const fetchDataMakanan = async () => {
        const result = await axios.get(`https://sinemaung-backend.herokuapp.com/makanan/makanan-list/`);
  
        setMakanan(result.data.map(x=>{return{id: x.id, nama: x.nama, harga: x.harga, deskripsi: x.desc, gambar: x.gambar}}));
      }
    

    const handleHarga = (event)=>{

        setCount(event.target.value)

    }
    const handleChange = (event)=>{
          var id = event.target.value
        setHargaSatuan(0)
        makanan.map((el)=>{
            if(id == el.id){

                setIdMakanan(el.id)
                setHargaSatuan(el.harga*count)
                alert("Makanan yang kamu pilih berhasil diubah kuantitasnya")
            }
        })
    }
    useEffect(()=>{
        handleProblem()
    },[hargaSatuan,idMakanan])
    const handleProblem = ()=>{
          listMakanan.map((el)=>{
              if(el.id ==idMakanan){
                  let tempArr = listMakanan
                let objIndex = tempArr.findIndex((obj => obj.id == el.id))
                tempArr[objIndex].harga = hargaSatuan
                setListMakanan(tempArr)

              }
              else {

                  var temp ={"id":idMakanan,"harga":hargaSatuan}
                setListMakanan([...listMakanan,temp])
              }

          })

    }


      const handleSubmit = (event) => {
          let tempArr = listMakanan.slice(2)
          let x = 0
          let makanan = []

          tempArr.map(el =>{
              let objIndex = tempArr.findIndex((obj => obj.id == el.id))
              x += tempArr[objIndex].harga
              makanan.push({id:tempArr[objIndex].id})
          })

          event.preventDefault()
         let tempUrl = window.location.href
            let tempId = tempUrl.substring(tempUrl.lastIndexOf('/')+1)
             axios.post(`https://sinemaung-backend.herokuapp.com/membeli/pembelian-update/${tempId}/`,
        {
            idReceipt: tempId,
            totalHarga:x,
            makanan:makanan
        })
          let tempUrl1 = window.location.href
            let tempId1 = tempUrl1.substring(tempUrl1.indexOf('/',53)+1,55)
        history.push(`/jadwal/${tempId1}/receipt/${tempId}`)
      }

    return(
        <>
        <div class="listmakanan">
        <h1>List Makanan</h1> 
        {   
        makanan.map((val, index)=>{
            return(
            <div class="card-makanan">
            <div class="gambarMakanan">
                <img src={val.gambar}/>
            </div>
            <div  class="details-makanan">
                <h3>{val.nama}</h3>
                <p>{val.deskripsi}</p>
                <p>Rp. {val.harga},-</p>
            </div>
            <div class="counter">
                <div class="buttons-container">
            <form>
            <label id="idMakanan" value={val.id}>
                <input id="inputCount"  onChange={handleHarga} min="0" type="number"/>
            </label>
                 <Button id="buttonMakanan" variant="primary" value={val.id} onClick={handleChange} >add</Button>{' '}
            </form>
                </div>
            </div>
        </div>
        )
        })
        
}
            <button class="button1" onClick={handleSubmit}>Beli</button>
            
        </div>
        </>
    )
}

export default ListMakanan;
