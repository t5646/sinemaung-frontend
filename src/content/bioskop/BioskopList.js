import React, { useEffect, useState } from "react"
import { useHistory } from "react-router"
import './BioskopList.css'
import img from './theatre.jpg'
import axios from "axios"

const BioskopList = () => {
    const [bioskops, setBioskops] = useState([])
    let history = useHistory()
    const rootUrl = "http://sinemaung-backend.herokuapp.com"

    useEffect(() => {
        console.log("Fetching List of Bioskop")
        axios.get(`${rootUrl}/bioskop/get`)
          .then( res => {
            setBioskops(res.data)
          }).catch(() => {
            alert("Failed fetching data")
        })
      },[])

    const handleScroll = () => {
        document.getElementById('bioskop-list').scrollIntoView()
    }

    const goToDetails = (event) => {
        let index = parseInt(event.target.value)
        history.push(`/bioskop/${index}`)
    }
    
    return(
        <div>
            <div className="content-bioskop">
                <div className="bioskop-title">
                   <span className="word-now">List Bioskop</span>
                </div>

                <div className="bioskop-list" id="bioskop-list">
                    {
                        bioskops.map((bio, index) => {
                            return (
                                <div className="bioskop-card" key={index+1}>
                                    <img src={img} alt="picture"/>
                                    <div className="container-ryanda">
                                        <h4><strong>{bio.nama}</strong></h4>
                                        <p>Location: {bio.alamat}</p>
                                        <p>{bio.jam_buka} - {bio.jam_tutup}</p>
                                        <button className="link-detail" value={bio.id} onClick={goToDetails}>&#62;&#62; See Details</button>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default BioskopList