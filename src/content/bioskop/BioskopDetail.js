import React, { useState, useEffect } from "react"
import { useHistory, useParams } from "react-router";
import img from './theatre.jpg'
import axios from "axios"

const BioskopDetail = () => {
    const {idBioskop} = useParams()
    const [nama, setNama] = useState("")
    const [alamat, setAlamat] = useState("")
    const [jam_buka, setJamBuka] = useState("")
    const [jam_tutup, setJamTutup] = useState("")
    const [list_of_studio, setListStudio] = useState([])
    let history = useHistory()
    const rootUrl = "http://sinemaung-backend.herokuapp.com"

    const goToDetails = (event) => {
        let index = parseInt(event.target.value)
        history.push(`/bioskop/${index}/jadwal`)
    }

    // useEffect(() => {
    //     let tempUrl = window.location.href
    //     let tempId = tempUrl.substring(tempUrl.lastIndexOf('/')+1)
    //     console.log("Fetching Bioskop")
    //     axios.get(`${rootUrl}/bioskop/get/${tempId}`)
    //       .then( res => {
    //         setBioskop(res.data)
    //       }).catch(() => {
    //         alert("Failed fetching data")
    //     })
    //   },[])

      useEffect(() => {
        const fetchData = async () => {
            let tempUrl = window.location.href
            let tempId = tempUrl.substring(tempUrl.lastIndexOf('/')+1)
            const result = await axios.get(`${rootUrl}/bioskop/get/${tempId}`)
            let temp = []
            setNama(result.data.nama)
            setAlamat(result.data.alamat)
            setJamBuka(result.data.jam_buka)
            setJamTutup(result.data.jam_tutup)
            if(result) {
                result.data.list_of_studio.map(el => {
                    temp.push(el.name)
                })
                setListStudio(temp)
            }
        }
        fetchData()
    }, [])

    return(
        <>
            <div className="content-details">
                <div className="bioskop-detail">
                    <img src={img} alt="picture"/>
                    <div className="detail">
                        <h2>{nama}</h2>
                        <h4>{jam_buka} - {jam_tutup}</h4>
                        <p>{alamat}</p>
                        <p>Daftar Studio:</p>
                        <p>
                            <ul>
                            {list_of_studio.map(studio => (
                                  <li>
                                 {studio}
                                 </li>
                             ))}
                            </ul>
                        </p>
                        <button className="link-detail" value={idBioskop} onClick={goToDetails}>&#62;&#62; Lihat Jadwal</button>
                    </div>
                </div>

            </div>
        </>
    )
}

export default BioskopDetail