import React, {useEffect, useState } from 'react'
import './Jadwal.css'
import 'bootstrap/dist/css/bootstrap.css'
import { useHistory, useParams } from 'react-router'
import axios from 'axios'

const Jadwal = () => {
    const {idBioskop} = useParams()
  const [jadwal, setJadwal] = useState([])
  let history = useHistory()
  const rootUrl = "http://sinemaung-backend.herokuapp.com"

  const redirectTo = (link)=> {
    history.push(link)
  }

  useEffect(() => {
    console.log("Fetching Jadwal")
    axios.get(`${rootUrl}/bioskop/get/${idBioskop}`)
      .then( res => {
          console.log(res.data.list_of_jadwal)
        setJadwal(res.data.list_of_jadwal)
      }).catch(() => {
        alert("Failed fetching data")
    })
  },[])

  const hitungBangkuTersedia = (daftar_bangku) => {
    let count = 0
    Object.values(daftar_bangku).forEach((bangku) => {
      if (bangku.isAvailable === true) {
        count += 1
      }
    })
    return count
  }

  return (
    <div>
      <h1 style={{marginLeft: "10%"}}>Jadwal</h1>
      {
        jadwal.map((item, index) => {
          return (
            <div className="jadwal-card" key={index} onClick={() => redirectTo('/jadwal/pembelian-tiket/'+ item.id)}>
              <img src={`https://image.tmdb.org/t/p/w500${item.film.poster}`} alt={index}></img>
              <div className="jadwal-details">
                <h4>{item.film.title}</h4>
                <p>{item.jam_tayang}</p>
                <p>Studio {item.studio}</p>
                <p>Rp {item.harga}</p>
                <p>Bangku tersedia: {hitungBangkuTersedia(item.daftar_bangku)}/{Object.keys(item.daftar_bangku).length}</p>
              </div>
            </div>
          )
        })
      }
    </div>
  )
}

export default Jadwal
