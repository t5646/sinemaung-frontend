import React, {useEffect, useState} from "react"
import layar from "../../static/layar.png"
import 'bootstrap/dist/css/bootstrap.css'
import { Form,Row,Col,Button,ButtonGroup } from 'react-bootstrap'
import './style-aimar.css'
import { useHistory } from "react-router"
import axios from "axios";
import Cookies from "js-cookie";



const PembelianTiket = () => {
    let history = useHistory()
    const [film, setFilm] = useState([])
    const [jamTayang, setJamTayang] = useState([])
    const [hargaFilm, setHargaFilm] = useState([])
    const [bangku, setBangku] = useState([])
    const [check, setCheck] = useState([])
    const [idReceipt, setIdReceipt] = useState('')
    const [bangkuTerpilih, setBangkuTerpilih] = useState([])
    const [id, setId] = useState('')
    const [user,setUser]=useState('')
    const [hargaTiket, setHargaTiket] = useState(0)
     useEffect(() => {
        const fetchData = async () => {
            let tempUrl = window.location.href
            let tempId = tempUrl.substring(tempUrl.lastIndexOf('/')+1)
            const result = await axios.get(`https://sinemaung-backend.herokuapp.com/jadwal/get/${tempId}/`)
            let arrBangku = []
                result.data.daftar_bangku.map(el => {
                    let temp = {
                        id:el.id,
                        chair : el.nomor,
                        value:el.isAvailable
                    }
                    arrBangku = [...arrBangku,temp]
                })
            setFilm(result.data.film.title)
            setJamTayang(result.data.jam_tayang)
            setHargaFilm(result.data.harga)
            setId(result.data.id)
            setBangku(arrBangku)
            setHargaTiket(0)
            setUser(Cookies.get('username'))

        }
            fetchData()

     },[])




    useEffect(()=>{
        makeid()
    },[])

    const makeid = () => {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < 10; i++ ) {
        result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
        };
         setIdReceipt(result)

    }



    const changeRow = (item) => {
        setCheck([])
        let namaRow = item.target.value
        let tempBangku = bangku.filter(el => el.chair[0] == namaRow)
        let tempArray = []
        tempBangku.map(item => {
            if (item.value) {
                tempArray.push(item.chair)
            }
        })
        setCheck(tempArray)
    }

    const handleChange = () => {
        if (document.getElementById('inputRow').value == 'Pilihan baris bangku'|| document.getElementById('inputNumber').value == 'Pilihan nomor bangku')
            alert("tolong isi form dengan benar")
        else{
             let selectedChair = document.getElementById('inputRow').value + '' + document.getElementById('inputNumber').value
            bangku.map(el => {
                if(el.chair == selectedChair) {
                    el.value = false
                    setBangkuTerpilih([...bangkuTerpilih,el.id])
                }
            })
            check.map(el => {
                if(el == selectedChair) {
                    setCheck(check.filter(item => item != selectedChair))
                }
            })
            alert("Bangku yang anda pilih berhasil ditambahkan")
            setHargaTiket(hargaTiket + hargaFilm)
            return document.getElementById('text').value += ' ' + selectedChair + ', '

        }
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        let tempArr = []
        bangkuTerpilih.map(function(a) {
            let res = {id: a}
            tempArr = [...tempArr, res]
        })
        axios.post(`https://sinemaung-backend.herokuapp.com/membeli/pembelian-create/`,
        {
            totalHarga: hargaTiket,
            idReceipt: idReceipt,
            pembeli:user,
            tiket: tempArr
        })
        .then(() => {
            history.push(`/jadwal/${id}/receipt/${idReceipt}`)
        })
        setBangkuTerpilih([])
    }

    const handleSubmitMakanan = (event) => {
        event.preventDefault()
        let tempArr = []
        bangkuTerpilih.map(function(a) {
            let res = {id: a}
            tempArr = [...tempArr, res]
        })
        axios.post(`https://sinemaung-backend.herokuapp.com/membeli/pembelian-create/`,
        {
            totalHarga: hargaTiket,
            idReceipt: idReceipt,
            pembeli:user,
            tiket: tempArr
        })
        .then(() => {
            history.push(`${id}/makanan/${idReceipt}`)
        })
        setBangkuTerpilih([])
    }

    return (
        <div className="container-aimar">
            <div className="top">
                <span className="bold">{film}</span>  {jamTayang}
                <div id="harga-div">
                    Rp{hargaFilm}
                </div>
                <div className="image-div">
                    <img src={layar} className="rounded mx-auto d-block" alt="broken" />
                </div>
            </div>
            <form >

            <Row className="mb-3">
            <Form.Group as={Col} controlId="formGridState">
            <Form.Label>Baris</Form.Label>
            <Form.Select id="inputRow" onChange={changeRow} defaultValue="Pilihan baris bangku">
                <option value="Pilihan baris bangku">Pilihan baris bangku</option>
                <option value={"A"}>A</option>
                <option value={"B"}>B</option>
                <option value={"C"}>C</option>
                <option value={"D"}>D</option>
                <option value={"E"}>E</option>
            </Form.Select>
            </Form.Group>
            </Row>

                 <Row className="mb-3">
            <Form.Group as={Col} controlId="formGridState">
            <Form.Label>Nomor</Form.Label>
            <Form.Select id="inputNumber" defaultValue="Pilihan nomor bangku">
                <option value="Pilihan nomor bangku">Pilihan nomor bangku</option>
                {
                    check.map(el => {
                        return(
                            <option value={el.substring(1)}>{el.substring(1)}</option>
                        )
                    })
                }
            </Form.Select>
            </Form.Group>
            </Row>
            <input type={"hidden"} id="idHarga" value={hargaTiket}/>
                <input type={"hidden"} id="idReceipt" value={idReceipt}/>
            <Form.Group className="mb-3"></Form.Group>
            <Button id="buttonTiket" variant="primary" onClick={handleChange} >add</Button>{' '}

                <div>
    <br/><br/><textarea readOnly id="text" style={{"height":"100px","width":"600px"}}></textarea>
</div>

</form >


<div>
    <h3 id="totalHarga">total harga: Rp {hargaTiket}</h3>
</div>
<ButtonGroup>
    <Button onClick={handleSubmit} variant="primary" active>Selesai</Button>{' '}
    <Button onClick={handleSubmitMakanan} variant="secondary">Membeli Makanan</Button>{' '}
</ButtonGroup>
    </div >
    )
}

export default PembelianTiket