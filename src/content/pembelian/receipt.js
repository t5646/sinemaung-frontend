import React, {useContext, useEffect, useState} from "react"
import './style-aimar.css'
import axios from "axios";
import { Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
const Receipt = () => {

    const [film, setFilm] = useState([])
    const [jamTayang, setJamTayang] = useState([])
    const [hargaTotal, setHargaTotal] = useState([])
    const [kode, setKode] = useState([])
    const [bioskop, setBioskop] = useState([])
    const [teater, setTeater] = useState([])
    const [hargaTiket, setHargaTiket] = useState(0)
    const [hargaMakanan, setHargaMakanan] = useState(0)
    const [username, setUsername] = useState('')
    const [bangku, setBangku] = useState([])
    const [nomorBangku,setNomorBangku] = useState('')
    const [makanan, setMakanan] = useState([])
    const [nomorMakanan,setNomorMakanan] = useState('')
    useEffect(() => {
        const fetchData = async () => {
            let tempUrl1 = window.location.href
            let tempId1 = tempUrl1.substring(tempUrl1.indexOf('/',37)+1,39)
            const result1 = await axios.get(`https://sinemaung-backend.herokuapp.com/jadwal/get/${tempId1}/`)
            let tempUrl = window.location.href
            let tempId = tempUrl.substring(tempUrl.lastIndexOf('/')+1)
            const result2 = await axios.get(`https://sinemaung-backend.herokuapp.com/membeli/receipt/${tempId}/`)
            axios.all([result1,result2]).then(
                axios.spread((...allData)=>{
                let arrBangku = []
                allData[1].data.tiket.map(el => {
                    let temp = {
                        id:el.id,
                        chair : el.nomor,
                        value:el.isAvailable
                    }
                    arrBangku = [...arrBangku,temp]
                })
                     let arrMakanan = []
                allData[1].data.makanan.map(el => {
                    let temp = {
                        id:el.id,
                        nama : el.nama
                    }
                    arrMakanan = [...arrMakanan,temp]
                })
                    setFilm(allData[0].data.film.title)
            setJamTayang(allData[0].data.jam_tayang)
                setBioskop(allData[0].data.bioskop.nama)
                    setTeater(allData[0].data.studio)
            setKode(allData[1].data.idReceipt)
            setHargaTotal(allData[1].data.totalHarga)
            setUsername(allData[1].data.pembeli)
                    setBangku(arrBangku)
                    setMakanan(arrMakanan)
               setHargaMakanan(allData[1].data.hargaMakanan)
                    setHargaTiket(allData[1].data.hargaTiket)
            })
            )
        }
            fetchData()


     },[])



      useEffect(()=>{
        handleBangku()
    },[bangku])
    const handleBangku = ()=>{
        let temp = ""
        for (let i = 0;i<bangku.length;i++){
             temp = temp+ bangku[i].chair+", "
        }
        setNomorBangku(temp)
    }

     useEffect(()=>{
        handleMakanan()
    },[makanan])
    const handleMakanan = ()=>{
        let temp = ""
        for (let i = 0;i<makanan.length;i++){
            temp = temp+makanan[i].nama+', '
        }
         setNomorMakanan(temp)
    }




    return(
        <table class="body-wrap">
    <tbody><tr>
        <td></td>
        <td class="container-aimar" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tbody><tr>
                        <td class="content-wrap aligncenter">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tbody><tr>
                                    <td class="content-block">
                                        <h2>Terima kasih telah menggunakan jasa kami</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <table class="invoice">
                                            <tbody><tr>
                                                <td>{username}<br/>Kode penukaran: {kode}<br/>{bioskop}<br/>{teater}<br/>{film}<br/> {jamTayang}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="invoice-items" cellpadding="0" cellspacing="0">
                                                        <tbody><tr>
                                                            <td>{nomorMakanan}</td>
                                                            <td class="alignright">Rp.{hargaMakanan}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>{nomorBangku}</td>
                                                            <td class="alignright">Rp.{hargaTiket}</td>
                                                        </tr>
                                                        <tr class="total">
                                                            <td class="alignright" width="80%">Total</td>
                                                            <td class="alignright">Rp.{hargaTotal}</td>
                                                        </tr>
                                                    </tbody></table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                </div>
        </td>
        <td></td>
    </tr>
</tbody></table>

    )
}

export default Receipt