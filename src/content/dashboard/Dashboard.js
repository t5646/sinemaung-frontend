import React from 'react'
import './Dashboard.css'
import 'bootstrap/dist/css/bootstrap.css'
import popcorn_img from './popcorn.jpg'
import bioskop_img from './bioskop.jpg'
import film_img from './film.jpg'
import { useHistory } from 'react-router-dom'
import { Carousel } from 'react-bootstrap'

function Dashboard() {
  let history = useHistory()

  const redirectTo = (link)=> {
    history.push(link)
  }

  return (
    <div className="dashboard">
      <p className="center title">Welcome to <span className="title bold">Sinemaung</span></p>
      <Carousel className="carousel center">
        <Carousel.Item onClick={() => redirectTo('/film')}>
          <img
            className="d-block w-100"
            src={film_img}
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>Daftar Film</h3>
            <p>Lihat semua film yang sedang kami tayangkan.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item onClick={() => redirectTo('/bioskop')}>
          <img
            className="d-block w-100"
            src={bioskop_img}
            alt="Second slide"
          />
          <Carousel.Caption>
            <h3>Daftar Bioskop</h3>
            <p>Lihat informasi tentang semua bioskop kami.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item onClick={() => redirectTo('/bioskop')}>
          <img
            className="d-block w-100"
            src={popcorn_img}
            alt="Third slide"
          />

          <Carousel.Caption>
            <h3>Makanan</h3 >
            <p>Anda dapat membeli tiket sekaligus makanan.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    
    </div>
  )
}

export default Dashboard
