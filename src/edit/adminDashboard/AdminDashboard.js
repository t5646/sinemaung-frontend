import React from 'react'
import { Button } from 'react-bootstrap'
import { useHistory } from 'react-router'
import '../../content/dashboard/Dashboard.css'

function AdminDashboard() {
  let history = useHistory()

  const redirectTo = (link)=> {
    history.push(link)
  }

  return (
    <div className="dashboard">
      <p className="center title">Admin Dashboard</p>
      <div className="d-grid gap-2 center" style={{marginTop: "48px", maxWidth: "500px"}}>
        <Button variant="success" size="lg" onClick={() => redirectTo('/admin/film')}>
          Film
        </Button>
        <Button variant="success" size="lg" onClick={() => redirectTo('/admin/bioskop')}>
          Bioskop
        </Button>
        <Button variant="success" size="lg" onClick={() => redirectTo('/admin/jadwal')}>
          Jadwal
        </Button>
        <Button variant="success" size="lg" onClick={() => redirectTo('/admin/makanan')}>
          Makanan
        </Button>
      </div>
    </div>
  )
}

export default AdminDashboard
