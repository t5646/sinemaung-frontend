import React, { useState, useEffect } from 'react'
import { useParams, useHistory, Switch, Route } from 'react-router'
import { Form, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import Select from 'react-select'
import axios from 'axios'

function FormJadwal() {
    const { idJadwal } = useParams()
    let history = useHistory()
    const [inputJam, setInputJam] = useState('')
    const [inputFilm, setInputFilm] = useState(0)
    const [inputBioskop, setInputBioskop] = useState(0)
    const [inputStudio, setInputStudio] = useState('')
    const [inputHarga, setInputHarga] = useState(0)
    const [inputBangku, setInputBangku] = useState(50)
    const [arrFilm, setArrFilm] = useState([])
    const [arrBioskop, setArrBioskop] = useState([])
    const [arrStudio, setArrStudio] = useState([])
    const rootUrl = "http://sinemaung-backend.herokuapp.com"

    // const fetchBioskop = async () => {
    //     axios.get(`${rootUrl}/bioskop/get`)
    //         .then( res => {
    //             let newArr = res.data.map((bioskop) => {
    //                 let res = { value: bioskop.id, label: bioskop.nama, studio: bioskop.list_of_studio, film: bioskop.list_of_film}
    //                 return res
    //             })
    //             setArrBioskop(newArr)
    //             return newArr
    //         }).catch(() => {
    //             alert("Failed fetching bioskop")
    //             return null
                
    //     })
    // }
  
    const handleFilmChange = (event) => {
        console.log(event.value)
        setInputFilm(event.value)
    }

    const handleHargaChange = (event) => {
        setInputHarga(event.target.value)
    }

    const handleBangkuChange = (event) => {
        setInputBangku(event.target.value)
    }

    const handleJamChange = (event) => {
        setInputJam(event.target.value)
    }

    const handleStudioChange = (event) => {
        setInputStudio(event.value)
    }

    const handleBioskopChange = async (event) => {
        let id_bioskop = event.value
        setInputBioskop(id_bioskop)
        let bioskop = arrBioskop.filter((element) => {
            console.log(element)
            return(element.value === id_bioskop)
        })
        setArrStudio(bioskop[0].studio.map((studio) => {
            let res = { value: studio.name, label: studio.name}
            return res
        }))
        setArrFilm(bioskop[0].film.map((film) => {
            let res = { value: film.id, label: film.title}
            return res
        }))
    }
    
    const handleSubmit = (event) => {
        event.preventDefault()
        if(window.location.pathname.toString().includes("/admin/jadwal/create")){
            axios.post(`${rootUrl}/jadwal/create/`,
            {
                "jam_tayang": inputJam,
                "studio": inputStudio,
                "harga": inputHarga,
                "bioskop": inputBioskop,
                "total_bangku": inputBangku,
                "film":inputFilm,
            }).then( res => {
                history.push('/admin/jadwal')
                alert("Berhasil membuat jadwal")
              }).catch(() => {
                alert("Gagal membuat jadwal")
            })
        }
        else if(window.location.pathname.toString().includes("/admin/jadwal/update")
        && (typeof idJadwal) != 'undefined') {
            axios.put(`${rootUrl}/jadwal/update/${idJadwal}`,
            {
                "jam_tayang": inputJam,
                "studio": inputStudio,
                "bioskop": inputBioskop,
                "harga": inputHarga,
                "film":inputFilm,
            }).then( res => {
                history.push('/admin/jadwal')
                alert("Berhasil mengubah jadwal")
              }).catch(() => {
                alert("Gagal mengubah jadwal")
            })
        }
    }

    const init = async () => {
        axios.get(`${rootUrl}/bioskop/get`)
            .then( res => {
                let arr_bioskop = res.data.map((bioskop) => {
                    let res = { value: bioskop.id, label: bioskop.nama, studio: bioskop.list_of_studio, film: bioskop.list_of_film}
                    return res
                })
                setArrBioskop(arr_bioskop)
                if (window.location.pathname.toString().includes("/admin/jadwal/update/")
                && (typeof idJadwal) != 'undefined') {
                    console.log("Fetching Jadwal")
                    axios.get(`${rootUrl}/jadwal/get/${idJadwal}/`)
                        .then( res => {
                            console.log(res.data)
                            setInputBioskop(res.data.bioskop.id)
                            console.log(arr_bioskop)
    
                            let bioskop = arr_bioskop.filter((element) => {
                                console.log(element)
                                return(element.value === res.data.bioskop.id)
                            })
                            setArrStudio(bioskop[0].studio.map((studio) => {
                                let res = { value: studio.name, label: studio.name}
                                return res
                            }))
                            setArrFilm(bioskop[0].film.map((film) => {
                                let res = { value: film.id, label: film.title}
                                return res
                            }))
                            
                            setInputFilm(res.data.film.id)
                            setInputHarga(res.data.harga)
                            setInputJam(res.data.jam_tayang)
                            setInputStudio(res.data.studio)
                        }).catch(() => {
                        alert("Failed fetching jadwal")
                    })
                }
            }).catch(() => {
                alert("Failed fetching bioskop")
                return null
                
        })
    }

    useEffect(() => {
        init()
    }, [])


  return (
    <div className="crud-jadwal">
        <Switch>
            <Route  exact path="/admin/jadwal/create"><h1>Create Jadwal</h1></Route>
            <Route  path="/admin/jadwal/update"><h1>Update Jadwal</h1></Route>
        </Switch>
        <br/>
        <Form onSubmit={handleSubmit}>
        <Form.Group>
          <Form.Label>Bioskop</Form.Label>
          <Select
                name="bioskops"
                options={arrBioskop}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={handleBioskopChange}
                value={arrBioskop.filter((item) => item.value === inputBioskop)[0]}
                required
            />
        </Form.Group>

        <Form.Group>
          <Form.Label>Studio</Form.Label>
          <Select
                name="studios"
                options={arrStudio}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={handleStudioChange}
                value={arrStudio.filter((item) => item.value === inputStudio)[0]}
                required
            />
        </Form.Group>
        <Form.Group>
            <Form.Label>Film</Form.Label>
            <Select
                name="genres"
                options={arrFilm}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={handleFilmChange}
                value={arrFilm.filter((item) => item.value === inputFilm)[0]}
                required = {true}
            />
        </Form.Group>
        
        <Form.Group>
          <Form.Label>Jam Tayang</Form.Label>
          <Form.Control value={inputJam} type="time" name="jam_tayang" onChange={handleJamChange} required>
          </Form.Control>
        </Form.Group>

        <Switch>
            <Route  exact path="/admin/jadwal/create">
                <Form.Group>
                <Form.Label>Jumlah Bangku</Form.Label>
                <Form.Control value={inputBangku} type="number" name="jumlah_bangku" onChange={handleBangkuChange} min={0} max={50} required>
                </Form.Control>
                </Form.Group>
            </Route>
        </Switch>


        <Form.Group>
          <Form.Label>Harga Tiket</Form.Label>
          <Form.Control value={inputHarga} type="number" name="harga_tiket" onChange={handleHargaChange} min={0} required>
          </Form.Control>
        </Form.Group>

        <br/>
        <br/>
        <Button variant="success" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  )
}

export default FormJadwal
