import React, { useContext, useEffect, useState } from 'react'
import { Link } from "react-router-dom";
import './AdminJadwal.css'
import 'bootstrap/dist/css/bootstrap.css'
import { Button } from 'react-bootstrap';
import axios from 'axios';

function AdminJadwal() {
  const [jadwal, setJadwal] = useState([])
  const rootUrl = "http://sinemaung-backend.herokuapp.com"

  const handleDelete = (event) => {
    let id = event.currentTarget.value
    axios.delete(`${rootUrl}/jadwal/delete/${id}`)
      .then(() => {
        let newJadwal = jadwal.filter(el=> {return el.id !== parseInt(id)})
        setJadwal(newJadwal)
        alert("Jadwal berhasil dihapus")
      }).catch(() => {
        alert("Jadwal gagal dihapus")
    })
  }

  const fetchData = async () => {
    console.log("Fetching Jadwal")
    axios.get(`${rootUrl}/jadwal/get/`)
      .then( res => {
        setJadwal(res.data)
        console.log(res.data)
      }).catch((error) => {
        console.log(error)
        alert("Failed fetching data")
    })
  }

  const hitungBangkuTersedia = (daftar_bangku) => {
    let count = 0
    Object.values(daftar_bangku).forEach((bangku) => {
      if (bangku.isAvailable === true) {
        count += 1
      }
    })
    return count
  }

  useEffect(() => {
    fetchData()
  }, [])


  return (
    <div className="crud-jadwal">
      <h1>Daftar Jadwal</h1>
      <Link to="/admin/jadwal/create"><Button variant='success'>Create Jadwal</Button></Link>
      <table>
        <thead>
          <tr>
          <th className="no">No</th>
          <th>Judul</th>
          <th>Jam</th>
          <th>Studio</th>
          <th>Bioskop</th>
          <th>Harga</th>
          <th>Jumlah Bangku</th>
          <th>Pilihan</th>
          </tr>
        </thead>
        <tbody>
          {
          jadwal.map((item, index) => {
              return (
              <tr key={item.id}>
                  <td className="no">{index + 1}</td>
                  <td>{item.film.title}</td>
                  <td>{item.jam_tayang}</td>
                  <td>{item.studio}</td>
                  <td>{item.bioskop.nama}</td>
                  <td>{item.harga}</td>
                  <td>{hitungBangkuTersedia(item.daftar_bangku)}/{Object.keys(item.daftar_bangku).length}</td>
                  <td>
                      <Link to={`/admin/jadwal/update/${item.id}`}><Button style={{backgroundColor:"#0d6efd"}} value={item.id}>Edit</Button></Link>
                      <Button variant="danger" onClick={handleDelete} value={item.id}>Delete</Button>
                  </td>
              </tr>
              )
          })
          }
        </tbody>
      </table>
    </div>
  )
}

export default AdminJadwal
