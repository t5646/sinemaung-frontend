import React, { useState } from "react"
import { useHistory } from "react-router"
import axios from "axios"
import Select from "react-select"

const FilmAddForm = () => {
    const [inputTitle, setInputTitle] = useState("")
    const [inputDesc, setInputDesc] = useState("")
    const [inputRating, setInputRating] = useState(0)
    const [inputDuration, setInputDuration] = useState(0)
    const [inputPoster, setInputPoster] = useState("")
    const [inputGenre, setInputGenre] = useState([])
    let history = useHistory();

    const options = [
        { value: 'Comedy', label: 'Comedy'},
        { value: 'Action', label: 'Action'},
        { value: 'Adventure', label: 'Adventure'},
        { value: 'Drama', label: 'Drama'},
        { value: 'Family', label: 'Family'},
        { value: 'Fantasy', label: 'Fantasy'},
        { value: 'Horror', label: 'Horror'},
        { value: 'Music', label: 'Music'},
        { value: 'Romance', label: 'Romance'},
        { value: 'Science Fiction', label: 'Science Fiction'},
    ]

    const handleTitleChanged = (event) => {
        let input = event.target.value
        setInputTitle(input)
    }

    const handleDescChanged = (event) => {
        let input = event.target.value
        setInputDesc(input)
    }

    const handleRatingChanged = (event) => {
        let input = event.target.value
        setInputRating(input)
    }

    const handleDurationChanged = (event) => {
        let input = event.target.value
        setInputDuration(input)
    }

    const handlePosterChanged = (event) => {
        let input = event.target.value
        setInputPoster(input)
    }

    const handleGenreChanged = (event) => {
        setInputGenre(Array.isArray(event) ? event.map(x => x.value) : [])
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        let tempArr = []
        inputGenre.map(function(a) {
            let res = {name: a}
            tempArr = [...tempArr, res]
        })

        axios.post(`https://sinemaung-backend.herokuapp.com/film/film-create/`,
        {
            title: inputTitle,
            desc: inputDesc,
            rating: inputRating,
            duration: inputDuration,
            poster: inputPoster,
            genres: tempArr
        })
        .then(() => {
            history.push(`/admin/film`)
        })

        setInputTitle("")
        setInputDesc("")
        setInputRating(0)
        setInputDuration(0)
        setInputPoster("")
        setInputGenre([])
    }

    return(
        <>
            <div className="content-form-add">
                <h1>Form Add Film</h1>
                <form className="form-film-add" onSubmit={handleSubmit}>
                    <table className="table-film-add">
                        <tbody>
                            <tr>
                                <td>
                                    <label>
                                        Judul:
                                    </label>
                                </td>
                                <td>
                                    <input type="text" value={inputTitle} onChange={handleTitleChanged} required /><br/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Deskripsi:
                                    </label>
                                </td>
                                <td>
                                    <textarea value={inputDesc} onChange={handleDescChanged} required /><br/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Rating:
                                    </label>
                                </td>
                                <td>
                                    <input type="number" value={inputRating} onChange={handleRatingChanged} min="1" max="10" required /><br/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Genre:
                                    </label>
                                </td>
                                <td>
                                    <Select
                                        isMulti
                                        name="genres"
                                        options={options}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        value={options.filter(obj => inputGenre.includes(obj.value))}
                                        onChange={handleGenreChanged}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Durasi:
                                    </label>
                                </td>
                                <td>
                                    <input type="number" value={inputDuration} onChange={handleDurationChanged} required /><br/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Link Poster:
                                    </label>
                                </td>
                                <td>
                                    <input type="text" value={inputPoster} onChange={handlePosterChanged} required /><br/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div className="centered-element">
                        <button className="button-submit-film">Submit</button>
                    </div>
                </form>
            </div>
        </>
    )
}

export default FilmAddForm