import React, { useEffect, useState } from "react"
import { useHistory } from "react-router";
import "./FilmEdit.css"
import axios from "axios";

const FilmTable = () => {
    const [film, setFilm] = useState([])
    const [trigger, setTrigger] = useState(true)
    let history = useHistory()

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://sinemaung-backend.herokuapp.com/film/film-list/`)
            setFilm(result.data.map(el => {
                let arrGenre = []
                el.genres.map(genre => {
                    arrGenre.push(`${genre.name}, `)
                })
                return {
                    id: el.id,
                    title: el.title,
                    desc: el.desc,
                    rating: el.desc,
                    duration: el.duration,
                    poster: el.poster,
                    genres: arrGenre
                }
            }))
            setTrigger(false)
        }
        if(trigger) {
            fetchData()
        }
    }, [trigger])

    const goToForm = () => {
        history.push(`/admin/film/create`)
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        history.push(`/admin/film/update/${index}`)
    }

    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        axios.delete(`https://sinemaung-backend.herokuapp.com/film/film-delete/${index}/`)
        .then(() => {
            setTrigger(true)
        }) 
    }

    return(
        <>
        { film !== null && (
            <div className="content-table">
                <h1>Daftar Film</h1>

                <button onClick={goToForm} className="button-tambah-film">Tambah film</button>
                <table className="table-film-list">
                    <thead>
                        <tr>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Rating</th>
                            <th>Genre</th>
                            <th>Durasi</th>
                            <th>Gambar</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            film.map((val, index) => {
                                return(
                                    <tr key={index}>
                                        <td style={{textAlign:"center"}}>{index+1}</td>
                                        <td>{val.title}</td>
                                        <td>{val.desc}</td>
                                        <td>{val.rating}</td>
                                        <td>{val.genres}</td>
                                        <td style={{textAlign:"center"}}>{val.duration}</td>
                                        <td>{val.poster}</td>
                                        <td>
                                            <button type="button" onClick={handleEdit} className="button-edit" value={val.id}>Edit</button>
                                            <button type="button" onClick={handleDelete} className="button-delete" value={val.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )}
        </>
    )
}

export default FilmTable