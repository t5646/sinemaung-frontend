import React, { useEffect, useState } from "react"
import { useHistory } from "react-router";
import "./BioskopEdit.css"
import axios from "axios";

const BioskopTable = () => {
    const [bioskops, setBioskops] = useState([])
    const [trigger, setTrigger] = useState(true)
    const rootUrl = "http://sinemaung-backend.herokuapp.com"
    let history = useHistory()

    const goToForm = () => {
        history.push(`/admin/bioskop/create`)
    }

    const handleDelete = (event) => {
        let id = event.currentTarget.value
        axios.delete(`${rootUrl}/bioskop/delete/${id}`)
          .then( res => {
            let newBioskops = bioskops.filter(el=> {return el.id !== parseInt(id)})
            setBioskops(newBioskops)
            alert("Bioskop berhasil dihapus")
            setTrigger(true)
          }).catch(() => {
            alert("Bioskop gagal dihapus")
        })
      }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        history.push(`/admin/bioskop/edit/${index}`)
    }

    // const fetchData = async () => {
    //     console.log("Fetching List of Bioskop")
    //     axios.get(`${rootUrl}/bioskop/get`)
    //       .then( res => {
    //         setBioskops(res.data)
    //       }).catch(() => {
    //         alert("Failed fetching data")
    //     })
    //   }

    // useEffect(() => {
    // fetchData()
    // }, [])

    useEffect(() => {
        const fetchData = async () => {
            console.log("Fetching List of Bioskop")
            const result = await axios.get(`${rootUrl}/bioskop/get`)
            setBioskops(result.data.map(el => {
                let arrStudio = []
                let arrFilm = []
                let arrJadwal = []
                el.list_of_studio.map(studio => {
                    arrStudio.push(`${studio.name}, `)
                })
                el.list_of_film.map(film => {
                    arrFilm.push(`${film.title}, `)
                })
                el.list_of_jadwal.map(jadwal => {
                    arrJadwal.push(`${jadwal.jam_tayang}(${jadwal.film.title}), `)
                })
                return {
                    id: el.id,
                    nama: el.nama,
                    alamat: el.alamat,
                    jam_buka: el.jam_buka,
                    jam_tutup: el.jam_tutup,
                    list_of_film: arrFilm,
                    list_of_jadwal: arrJadwal,
                    list_of_studio: arrStudio
                }
            }))
            setTrigger(false)
        }
        if(trigger) {
            fetchData()
        }
    }, [trigger])

    return(
        <>
            <div className="content-table">
                <h1>Daftar Bioskop</h1>

                <button onClick={goToForm} className="button-tambah-bioskop">Tambah bioskop</button>
                <table className="table-bioskop-list">
                    <thead>
                        <tr>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Jam Buka</th>
                            <th>Jam Tutup</th>
                            <th>List Studio</th>
                            <th>List Film</th>
                            <th>List Jadwal</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        bioskops.map((bio, index) => {
                            return (
                                <tr key={bio.id}>
                                    <td>{index+1} + {bio.id}</td>
                                    <td>{bio.nama}</td>
                                    <td>{bio.alamat}</td>
                                    <td>{bio.jam_buka}</td>
                                    <td>{bio.jam_tutup}</td>
                                    <td>
                                        <ul>
                                        {bio.list_of_studio.map(studio => (
                                            <li>
                                            {studio}
                                            </li>
                                        ))}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                        {bio.list_of_film.map(film => (
                                            <li>
                                            {film}
                                            </li>
                                        ))}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                        {bio.list_of_jadwal.map(jadwal => (
                                            <li>
                                            {jadwal}
                                            </li>
                                        ))}
                                        </ul>
                                    </td>
                                    <td><button type="button" onClick={handleEdit} className="button-edit" value={bio.id}>Edit</button>
                                    <button variant="danger" type="button" onClick={handleDelete} className="button-delete" value={bio.id}>Delete</button></td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default BioskopTable