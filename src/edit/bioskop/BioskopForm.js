import React, { useState, useEffect } from 'react'
import { useParams, useHistory, Switch, Route } from 'react-router'
import { Form, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import Select from 'react-select'
import axios from 'axios'

const BioskopForm = () => {
    const { idBioskop } = useParams()
    const [inputNama, setInputNama] = useState("")
    const [inputAlamat, setInputAlamat] = useState("")
    const [inputJamBuka, setInputJamBuka] = useState("")
    const [inputJamTutup, setInputJamTutup] = useState("")
    const [inputListFilm, setInputListFilm] = useState([])
    const [inputListStudio, setInputListStudio] = useState("")
    const [arrListFilm, setArrListFilm] = useState([])
    let history = useHistory();
    const rootUrl = "http://sinemaung-backend.herokuapp.com"
    
    const fetchFilms = async () => {
        axios.get(`${rootUrl}/film/film-list/`)
            .then( res => {
                let newFilms = res.data.map((film) => {
                    let res = { label: film.title, value: film.id}
                    return res
                })
                setArrListFilm(newFilms)
            }).catch(() => {
            alert("Failed fetching data")
        })
    }

    const handleNamaChange = (event) => {
        console.log(event.value)
        setInputNama(event.target.value)
    }

    const handleAlamatChange = (event) => {
        setInputAlamat(event.target.value)
    }

    const handleJamBukaChange = (event) => {
        setInputJamBuka(event.target.value)
    }

    const handleJamTutupChange = (event) => {
        setInputJamTutup(event.target.value)
    }

    const handleListFilmChange = (event) => {
        setInputListFilm(Array.isArray(event) ? event.map(film => film.value) : [])
    }

    const handleListStudioChange = (event) => {
        setInputListStudio(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if(window.location.pathname.toString().includes("/bioskop/create")){
            axios.post(`${rootUrl}/bioskop/create`,
            {
                "nama": inputNama,
                "alamat":inputAlamat,
                "jam_buka": inputJamBuka,
                "jam_tutup": inputJamTutup,
                "list_of_studio": inputListStudio,
                "list_of_film":inputListFilm,
            }).then( res => {
                alert("Berhasil membuat bioskop")
              }).catch(() => {
                alert("Gagal membuat bioskop")
            })
            history.push('/admin/bioskop')
        }
        else if(window.location.pathname.toString().includes("/admin/bioskop/edit")) {
            axios.put(`${rootUrl}/bioskop/update/${window.location.href.substring(window.location.href.lastIndexOf('/')+1)}`,
            {
                "nama": inputNama,
                "alamat":inputAlamat,
                "jam_buka": inputJamBuka,
                "jam_tutup": inputJamTutup,
                "list_of_studio": inputListStudio,
                "list_of_film":inputListFilm,
            }).then( res => {
                alert("Berhasil mengubah bioskop")
              }).catch(() => {
                alert("Gagal mengubah bioskop")
            })
            history.push('/admin/bioskop')
        }
    }

    const init = () => {
        fetchFilms()
        if (window.location.pathname.toString().includes("/admin/bioskop/edit") /*&& (typeof idBioskop) != 'undefined'*/) {
            // let idBioskop = window.location.href.substring(window.location.href.lastIndexOf('/')+1)
            console.log("Fetching Bioskop")
            axios.get(`${rootUrl}/bioskop/get/${window.location.href.substring(window.location.href.lastIndexOf('/')+1)}`)
                .then( res => {
                    console.log(res.data)
                    setInputNama(res.data.nama)
                    setInputAlamat(res.data.alamat)
                    setInputJamBuka(res.data.jam_buka)
                    setInputJamTutup(res.data.jam_tutup)
                    setInputListStudio(res.data.list_of_studio.map((studio) => {
                        let res = `${studio.name}`
                        return res
                    }))
                    setInputListFilm(res.data.list_of_film.map((film) => {
                        let res = film.id
                        return res
                    }))
                }).catch(() => {
                alert("Failed fetching data")
            })
        }
    }

    useEffect(() => {
        init()
    }, [])

    return(
        <div className="ud-bioskop">
        <Switch>
            <Route  exact path="/admin/bioskop/create"><h1>Create Bioskop</h1></Route>
            <Route  path="/admin/bioskop/edit"><h1>Update Bioskop</h1></Route>
        </Switch>
        <Form onSubmit={handleSubmit}>
            <Form.Group>
            <Form.Label>Nama</Form.Label>
            <Form.Control value={inputNama} type="string" name="nama" onChange={handleNamaChange} required>
            </Form.Control>
        </Form.Group>
        
        <Form.Group>
          <Form.Label>Jam buka</Form.Label>
          <Form.Control value={inputJamBuka} type="time" name="jam_buka" onChange={handleJamBukaChange} required>
          </Form.Control>
        </Form.Group>

        <Form.Group>
          <Form.Label>Jam tutup</Form.Label>
          <Form.Control value={inputJamTutup} type="time" name="jam_tutup" onChange={handleJamTutupChange} required>
          </Form.Control>
        </Form.Group>

        <Form.Group>
            <Form.Label>Alamat</Form.Label>
            <Form.Control value={inputAlamat} type="string" name="alamat" onChange={handleAlamatChange} required>
            </Form.Control>
        </Form.Group>

        <Form.Group>
          <Form.Label>Film</Form.Label>
          <Select
                name="list_of_film"
                isMulti
                placeholder="Select Film"
                options={arrListFilm}
                className="basic-multi-select"
                classNamePrefix="select"
                value={arrListFilm.filter((item) => inputListFilm.includes(item.value))}
                defaultValue={arrListFilm.filter((item) => inputListFilm.includes(item.value))}
                onChange={handleListFilmChange}
                required
            />
        </Form.Group>

        <Form.Group>
          <Form.Label>Studio</Form.Label>
          <Form.Control value={inputListStudio} type="string" name="list_of_studio" onChange={handleListStudioChange} required>
        </Form.Control>
        </Form.Group>

        <br/>
        <Button variant="success" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  )
}

export default BioskopForm