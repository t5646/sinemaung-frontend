import React, {useState, useEffect} from "react";
import {useHistory, useParams} from "react-router-dom";
import axios from "axios";
const MakananFormEdit = () =>{
    let history = useHistory();
    const {idMakanan} = useParams();
    const [id, setId] = useState("");
    const [inputNama, setInputNama] =useState("");
    const [inputHarga, setInputHarga] =useState(0);
    const [inputDeskripsi, setInputDeskripsi] =useState("");
    const [inputGambar, setInputGambar] =useState("");
    const style ={
      width: "100%",
      padding: "12px 20px",
      margin: "8px 0",
      boxSizing: "border-box"
    }

    useEffect(() => {
      fetchDataMakanan();
    }, [id])

    const fetchDataMakanan = async () => {
      // const result = await axios.get(`http://localhost:8000/makanan/makanan-list/`);

      // setMakanan(result.data.map(x=>{return{id: x.id, nama: x.nama, harga: x.harga, deskripsi: x.desc, gambar: x.gambar}}));
      const result = await axios.get(`https://sinemaung-backend.herokuapp.com/makanan/makanan-detail/${idMakanan}/`)
      setInputNama(result.data.nama)
      setInputHarga(result.data.harga)
      setInputDeskripsi(result.data.desc)
      setInputGambar(result.data.gambar)
      console.log(result.data.nama)
  }

    const handleChange = (event) => {
      if(event.target.name === "inputName"){
        setInputNama(event.target.value);
      } else if (event.target.name === "inputHarga"){
        setInputHarga(event.target.value);
      } else if (event.target.name === "inputDeskripsi"){
        setInputDeskripsi(event.target.value);
      } else if (event.target.name === "inputGambar"){
        setInputGambar(event.target.value)
      }
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    // if(id === ""){
      axios.put(`https://sinemaung-backend.herokuapp.com/makanan/makanan-update/${idMakanan}/`, {nama: inputNama, harga: inputHarga, desc: inputDeskripsi, gambar: inputGambar})
        .then(res=>{
        //   let data = res.data
        //   setMakanan([...makanan],{id: data.id, nama: data.nama, harga: data.harga, desc: data.desc, gambar: data.gambar})
        history.push('/admin/makanan')
        })
    // } else {
    //   axios.post(`http://localhost:8000/makanan/makanan-update/${id}`, {nama: inputNama, harga: inputHarga, desc: inputDeskripsi, gambar: inputGambar})
    //   // .then(()=>{
    //   //   let singleMakanan = makanan.find(el=>el.id === id);
    //   //   singleMakanan.nama = inputNama;
    //   //   singleMakanan.harga = inputHarga;
    //   //   singleMakanan.desc = inputDeskripsi;
    //   //   singleMakanan.gambar = inputGambar;
    //   //   setMakanan([...makanan])
    //   // })
    // }
    setInputNama("");
    setInputHarga(0);
    setInputDeskripsi("");
  }
    
    return (
        <>
 {/* Form */}
 <div className="formPeserta">
 <h1>Form Edit Makanan</h1>
 <button className="button" onClick={()=>{history.push('/admin/makanan')}}>kembali</button>
 <form onSubmit={handleSubmit}>
   <label>
     Nama:
   </label>
   <input style={style} type="text" name="inputName" value={inputNama} onChange={handleChange} required/>
   <label>
     Harga:
   </label>
   <input style={style} type="number" name="inputHarga" value={inputHarga} onChange={handleChange} required />
   <label>
     Deskripsi:
   </label>
   <input style={style} type="text" name="inputDeskripsi" value={inputDeskripsi} onChange={handleChange} required/>
   <label>
     Image URL:
   </label>
   <input style={style} type="text" name="inputGambar" value={inputGambar} onChange={handleChange} required/>
   <button className="button">submit</button>
 </form>
 </div>
      </>
    )
    
}

export default MakananFormEdit;
