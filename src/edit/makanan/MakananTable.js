import React, {useState, useEffect} from "react";
import {useHistory } from "react-router-dom";
import './makanan.css'
import axios from "axios";

const MakananTable = () =>{
      const [makanan, setMakanan] = useState([]);
      // const [id, setId] = useState("");
      // const [inputNama, setInputNama] =useState("");
      // const [inputHarga, setInputHarga] =useState(0);
      // const [inputDeskripsi, setInputDeskripsi] =useState("");
      // const [inputGambar, setInputGambar] =useState("");
      let history = useHistory();



    useEffect(() => {
      fetchDataMakanan();
    }, [])

    const fetchDataMakanan = async () => {
      const result = await axios.get(`https://sinemaung-backend.herokuapp.com/makanan/makanan-list/`);

      setMakanan(result.data.map(x=>{return{id: x.id, nama: x.nama, harga: x.harga, deskripsi: x.desc, gambar: x.gambar}}));
    }

    const handleDelete = (event) => {
      let index = parseInt(event.target.value);
      axios.delete(`https://sinemaung-backend.herokuapp.com/makanan/makanan-delete/${index}/`)
      
      .then(()=>{
        let newMakanan = makanan.filter(el=>{return el.id !== index});
        setMakanan(newMakanan)
        console.log("test")
      })
    }


    const handleEdit = (event) => {
      let index = parseInt(event.target.value);
      // axios.get(`http://localhost:8000/makanan/makanan-update/${index}`)
      history.push(`/admin/makanan/update/${index}/`)
    }

    
    return (
        <>
        <div className="daftar">
        <h1>Daftar Makanan</h1>
        <button className="button" onClick={()=>{history.push('/admin/makanan/create')}}>Tambah Data Makanan</button>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Deskripsi</th>
              <th>Gambar</th>
            </tr>
          </thead>
          <tbody>
  
            {
              makanan.map((val, index) => {
                return (
                  <tr>
                    <td>{index + 1}</td>
                    <td>{val.nama}</td>
                    <td>Rp. {val.harga}</td>
                    <td>{val.deskripsi}</td>
                    <td><img src={val.gambar}></img></td>
                    <td>
                        <button className="button" onClick={handleEdit} value={val.id}>Edit</button>
                        <button className="button" onClick={handleDelete} value={val.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
  
          </tbody>
        </table>
        </div>

       
      </>
    )
    
}

export default MakananTable;