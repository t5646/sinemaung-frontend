import logo from './logo.svg';
import './App.css';
import Main from './main/Main';
import { BrowserRouter as Router } from "react-router-dom";
import { UserProvider } from './auth/UserContext';

function App() {
  return (
    <UserProvider>
      <Router>
        <Main />
      </Router>
    </UserProvider>
  );
}

export default App;
